v1.0.2 (2023-04-18)
	Game version: 1.35
	
	Minor changes:
		Clarified compatibility with EU4 v1.35

v1.0.1 (2023-01-09)
	Game version: 1.34

	Minor changes:
		Fixed 2 instances of double quotation marks in localization
		Added changelog
		
v1.0.0 (2022-10-31)
	Game version: 1.34

	Initial release